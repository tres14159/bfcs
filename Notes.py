#Bright Flame Tactics
 




Networking:
    1) Ideas of how to pull it off
        0)list of actions:
            give a list of actions to take i.e. ['move', 'attack'] or ['attack', 'kill', 'level up'] with a second list of args and parse these at the other client
            
        A) Movement:
            implement position recording of every position the player moves to.
            add a movelist = None parameter for movement phase. if a movelist is past the player does this, with a special END case, like a variable indicating what the next phase is
            eventually send moves to server realtime
        B) Fighting:
            pass the result of fight to the other client and do the fight animation phase.
            


























 
'''
Skills (special abilities, require a full skill bar?):
    No turn cost:
    Enrage: +30% chance of critical hit, 5 turns
    Focus: +100% attack success, +20% dodge, 3 turns
    
    1 turn cost:
    (going away maybe) Steal: Steal random item from selected opponent. range 1, 75% chance of success, 100% if enemy has < 50% of their total hp
    Meditate: +50% dodge, 4 turns
    Chain Luck: if any friendly gets lucky, doubles user's luck stat(compound). lasts for 15 turns, after which luck stat returns to normal
    
Character Classes:
    Swordsman: can use any sword. direct combat only. good strength and defense
    Lanceman: any spear. good stregth
    Archer: Weak with direct combat. dagger, any bow
    Cavalry: Mounted, average strength. 1-handed swords, lances
    (going away probably) Scout: Mounted, Fast mvt, weak, ignores ZOC. 1-handed swords, light bows
    Mage: healing + magic, slow mvt, low def, low strength, dagger, any spell, ignores ZOC

    Class weapon choices:
        Swordsman:
            Primary: Rapier, Broadsword, Longsword, Zweihander
            Secondary: Dagger, Shortbow, Spontoon, Rapier
            
        Spearman:
            Primary: Pike, Lance, Javelin, Longspear
            Secondary: Dagger, Shortbow, Spontoon, Javelin
            
        Archer:
            Primary: Longbow, Recurve Bow, Heavy Bow, Crossbow
            Secondary: Dagger, Shortbow, Spontoon Crossbow
            
        Mage: 
            Primary: Fireball, Icebeam, Lightning, Earthquake
            Secondary: Dagger, Heal, Lightning, Earthquake
      
            
    
Battles:
    Movement:
        Highlighting:
            -landable squares blue and attackable squares red. Yellow or green or maybe just blue squares can be moved through but not landed on (friendly units)
            # -this can be done with an overlay squares that have an alpha value of probably 30-70% 
            # -was going to use a graph primitive but ended uip doing bfs on a list using a queue
     
        Active Movement: 
            -will allow the player to move freely with the arrow keys within the mvt range
            -this is like shining force
     
    Combat:
        Support Bonus:
            -consider implementing some support bonus like Nectaris
            -units attack/defense are better when there are adjacent friendlies:
                -when attacking, friendlies adjacent to target provide support bonus
                -when defending, friendlies adjacent to defender provide support bonus
                
        # Primary vs Secondary Weapon:
            # -attack with either, defend only with primary?
            # -counterattack with primary if possible, secondary if primary cant
            
        Crit:
			critical hit doubles damage.
			'critical' defense is just a dodge
		
		# 2nd attack after counter:
			# need to do some kind of calculation (spd and skl maybe) to determine if there is a 2nd attack after the counter.
    
Weapons:
    need to implement a METHOD that returns core and aux stat so I can override it for heavy bow
    for walk, just repeat the first frame but have the horse move (for cavalry)
    

    Swords:
        dagger - light, any character can use a dagger
        Rapier - Piercing, light, less damage than broadsword but partially ignores armor rating
        Broadsword - basic sword
        
        Longsword - 2 handed, better damage than Zweihander, heavy
        !Axe - 
    
    
    spears: piercing. partially negates armor
        pike - standard spear type. possibly more effective against mounted units but the current bonus system doesnt allow for this and idc about it
        lance - heavy, big and metal
    
        #!javelin: ranged and direct combat
        !Trident: awesome piercing stat
    
    
    bows: except for crossbow, can attack units behind other units/obstacles. tall obstacles (trees) different? (probably not)
        ShortBow - light, range 1-2, dmg 1
        !longbow - standard, range 2-3, dmg 1-2
        !recurve bow - standard, range 1-2, dmg 2
        #!heavy bow - like a longbow but can be used in direct combat. heavy, range 1, dmg 2 (direct); range 2-3, dmg 2-3 (ranged). blue metal.
        #!crossbow - standard, range 1-2, dmg 3-4 cannot shoot over units
    
    
    spells:
        fire - light splash damage
        ice - possibility of freezing enemy for 1 turn
        !heal
        lightning - ignores armor bonus
        Tornado - AOE spell
    
    
    
    misc ideas:
        heavy weapon requires some strength stat
        perhaps weapon defense bonuses only apply to incoming direct combat attacks i.e. youre not going to reduce the strength of an arrow with a broadsword
        character creation: use q- stat bar +w  where q is decrement and w is increment. etc for all stats
        
Coding ideas:
        invert player's affinity for heal, then put it back
        append a None to the weapon list for any slots not available, and then dont let it be selected.
        
        
        
      

Game Flow Outline:
1) Launch/Startup
    0) LICENSE - black text on white a la gamecube warning - 18pt
        1) This game is released under GPL v3
            a) which means that you are free to do as you wish with the game, 
            source code, and ideas herein, as long as distribution includes all the 
            official sounding stuff that you can read in Copying.txt if you wish to 
            know the details.
            
            b) - new screen: special thanks to {logos} Creative Commons, OpenGameArt, and the Free Software Foundation for
            being a part of this, and a VERY special thanks to the minds and team behind the first ever...
            
           
      
    A) Opening Credits - colorful logos Fade in on black screen {going for 'classssic'}
        1) LiberatedPixelCup
            a) pixel breaks free from chains and hops along, jumping and landing on the 'i'
            b) Liberated pixel Cup Logo
        
        2) gr3yh47
            a) cap logo
            b) Imagined, written, and coded by gr3yh47
        
        3) Fade to black
            
    B) TITLE SCREEN - fade in from black (title on white background)
        a) GAME LOGO
        b) smaller plain text blinking 'press space or enter'
        
2) Main Menu:
    a) 3 buttons:
        1) 1 and 2 player buttons, silhouettes like some arcade buttons 
            a) they are the same square buttons as the rest of the menu and are next to eachother
        2) question mark button, circle
            a) below and centered
            b) text under it "this button provides help at any time during the game. 
                               it will be in the upper right corner after this menu
    

3) 2 player game:
    A) [1p], customize your captain:
        1) Custom character menu
            a) choose your....
                0) gender
                    a) m or f
                1) name
                    a) 5 or so choices each randomly selected from 50000 surnames
                2) class
                3) primary weapon
                4) secondary weapon
                5) stats
                6) leadership style
                    a) power + 1 to supporting cast str stat
                        1) every supporting cast screen has a swordsman, horseman, and spearman option. 
                        2) 2 archers, one with a mage and one without, with the others as well
                    b) strategy + 1 to supporting cast int stat
                        1) 2 archer and 3 mages each show up, once together and each their own. one horse one of the mages. 
                        2) 1 sword offered with the horse and the mage
                        3) 1 spear offered with the other mage
                    c) efficiency: + 1 to supporting cast spd stat
                        1) 3 horses offered and 2 arrows, once together and each their own. one mage with the 2 together.
                        2) 2 screens have spear and sword together
                    d) fortune: +2 to supporting cast lck stat
                        1) every class offered for every slot
                
            b) supporting cast:
                1) first choice of 2 or 3
                    a) each has an assigned name randomly chosen from 50000 surnames. et cetera for below.
                    b) classes will be static based on captain style chosen in a)6)
                    c) weapons will be pop up as available based on the character selected.
                2) second choice of 2
                3) third choice of 4
                4) fourth choice of 5
    
    B) Call A) for [2p]
    
    C) Map select:
        1) vertical column of buttons on right with black text above each one (map title)
        2) map display on left
     
    D) BATTLE
        
4) 1 player game:
    A) Customize your team
        See 3)A)1)
            
                

    
            
        
        
 '''       
        '''stats: 
            self.stats[str] = stats[str] #strength
            self.stats[dfn] = stats[dfn] #defense
            self.stats[spd] = stats[spd] #speed
            self.stats[int] = stats[int] #intelligence
            self.stats[lck] = stats[lck] #luck
            self.stats[skl] = stats[skl] #skill (stat) might change to or be used as dexterity 
            self.stats[mvt] = stats[mvt] #movement
            self.stats[lvl] = stats[lvl] #level
            self.stats[max] = stats[max] #max hp
            self.stats[nxt] = stats[nxt] total xp needed for next level'''
            
            