import re

def main():
    file_list = []
    name = ' '
    while name:
        name = raw_input('file name or return to end: ')
        if name:
            file_list.append(name)
    
    f_list = []
    for filename in file_list:
        #try:
        f = open(".\\" + filename)
        f_list.append(f)
        #except:
        #    print "file not found, continuing"
    
    line_count = 0
    passing = 0
    for f in f_list:
        for line in f:
            line = line.strip()
            if line == '':
                pass
            else:
                
                if line[0] == '#' or (line[0:3] == r"'''" and line [-3:] == r"'''"):
                    pass
                elif line[0:3] == "'''" or re.sub(r"'''", "", line) != line:
                    if passing == 1:
                        passing = 0
                    else:
                        passing = 1
                if passing == 1:
                    pass
                else:
                    line_count += 1
        
    print "line count is: ", line_count
    
    
    
main()
                