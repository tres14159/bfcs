# Bright Flame Tactics (tentative title)
#
# a turn-based tactical strategy game heavily inspired by the games Fire Emblem and Shining Force
#
# will be entered in the liberated pixel cup. as of June 4, 2012 all source is licensed under GPL v3
# see COPYING.txt for details
#
# Free use, do as you like with it, but any derivatives must be GPL v3. see COPYING.txt for legal details